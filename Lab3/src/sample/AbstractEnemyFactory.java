package sample;

public interface AbstractEnemyFactory {

    Enemy createEnemy();
    Enemy createBoss();
}
