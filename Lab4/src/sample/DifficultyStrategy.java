package sample;

import java.util.List;

public interface DifficultyStrategy {

    void execute(List<Enemy> enemies);

    EnemyBoss execute(int killedEnemies);

    int difficultyMultiplier(int score);
}
