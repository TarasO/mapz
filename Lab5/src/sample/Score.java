package sample;

public class Score {

    private String name;
    private String difficulty;
    private int score;

    public Score(String name, String difficulty, int score) {
        this.name = name;
        this.difficulty = difficulty;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
