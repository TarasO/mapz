package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main extends Application {

    AbstractEnemyFactory enemyFactory = new EasyEnemyFactory();

    @Override
    public void start(Stage primaryStage) throws Exception{

        primaryStage.setTitle("Hello World");

        Group root = new Group();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        Canvas canvas = new Canvas( 1280, 768 );
        root.getChildren().add(canvas);

        ArrayList<String> input = new ArrayList<String>();

        scene.setOnKeyPressed(
                new EventHandler<KeyEvent>()
                {
                    public void handle(KeyEvent e)
                    {
                        String code = e.getCode().toString();
                        if(code.equals("ENTER")) {
                            System.out.println();
                        }
                        if (!input.contains(code))
                            input.add(code);
                    }
                });

        scene.setOnKeyReleased(
                new EventHandler<KeyEvent>()
                {
                    public void handle(KeyEvent e)
                    {
                        String code = e.getCode().toString();
                        input.remove(code);
                    }
                });

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Font font = Font.font( "Helvetica", FontWeight.BOLD, 24 );
        gc.setFont(font);
        gc.setFill(Color.YELLOW);
        gc.setLineWidth(1);

        Image space = new Image(getClass().getClassLoader().getResource("space.jpg").toString());

        PlayerShip player = new PlayerShip(new Armament(new SingleGun()));
        List<Enemy> enemies = new ArrayList<>();

        LongValue lastNanoTime = new LongValue(System.nanoTime());
        IntValue score = new IntValue(0);

        EncryptedFileWriter encryptedFileWriter = new EncryptedFileWriter("score.txt");

        String prevScore = encryptedFileWriter.readFile();

        System.out.println(prevScore);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                encryptedFileWriter.writeFile(String.valueOf(score.value));
            }
        });

        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                double elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
                lastNanoTime.value = currentNanoTime;

                gc.drawImage( space, 0, 0 );

//                if(score.value > 2000) {
//                    enemyFactory = YellowowlFactory.getInstance();
//                }

                if(enemies.size() < 5) {
                    enemies.add(enemyFactory.createEnemy());
                }

                Iterator<Enemy> enemiesIter = enemies.iterator();
                while(enemiesIter.hasNext()){
                    Enemy enemy = enemiesIter.next();
                    enemy.update(elapsedTime);
                    enemy.render(gc);
                    if(enemy.getSprite().getPositionY() > 790) {
                        enemiesIter.remove();
                    }

                    if(player.hitBody(enemy)){
                        enemiesIter.remove();
                        player.setLife(player.getLife() - enemy.getDamage());
                    }
                }

                player.getSprite().setVelocity(0,0);
                if (input.contains("LEFT"))
                    player.getSprite().addVelocity(-250,0);
                if (input.contains("RIGHT"))
                    player.getSprite().addVelocity(250,0);
                if (input.contains("UP"))
                    player.getSprite().addVelocity(0,-250);
                if (input.contains("DOWN"))
                    player.getSprite().addVelocity(0,250);
                if (input.contains("SPACE")){
                    player.getArmament().shoot(player);
                }

                for(int i = 0; i < player.getArmament().getBullets().size(); i++) {
                    Sprite bullet = player.getArmament().getBullets().get(i);
                    bullet.update(elapsedTime);
                    bullet.render(gc);

                    if(bullet.getPositionY() < 0) {
                        player.getArmament().getBullets().remove(i);
                    }

                    if(player.getArmament().hit(enemies)) {
                        score.value += 50;
                    }
                }

                player.update(elapsedTime);
                player.render(gc);

                player.getArmament().reduceCooldown(5);

                String lifeText = "Life:" + player.getLife();
                gc.fillText(lifeText, 100,30);
                String pointsText = "Score:" + score.value;
                gc.fillText(pointsText, 1100, 30);
                if(prevScore != null) {
                    gc.fillText("Prev. Score:" + prevScore, 1070, 60);
                }
            }
        }.start();

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
