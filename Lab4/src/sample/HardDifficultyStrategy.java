package sample;

import java.util.List;

public class HardDifficultyStrategy implements DifficultyStrategy {

    private AbstractEnemyFactory enemyFactory;

    HardDifficultyStrategy() {
        enemyFactory = new HardEnemyFactory();
    }

    @Override
    public void execute(List<Enemy> enemies) {
        if (enemies.size() < 5) {
            enemies.add(enemyFactory.createEnemy());
        }
    }

    @Override
    public EnemyBoss execute(int killedEnemies) {
        if(killedEnemies >= 20) {
            return enemyFactory.createBoss();
        }
        return null;
    }

    @Override
    public int difficultyMultiplier(int score) {
        return (int)Math.round(score * 1.5);
    }
}
