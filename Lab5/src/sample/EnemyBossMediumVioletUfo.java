package sample;

public class EnemyBossMediumVioletUfo extends EnemyBoss {

    public EnemyBossMediumVioletUfo() {
        this.sprite = new Sprite();
        this.sprite.setImage(SpriteImageHolder.violetUfo);
        this.sprite.setPosition(640, -200);
        this.sprite.addVelocity(0, 0);

        this.life = 500;
        this.damage = 60;
    }
}
