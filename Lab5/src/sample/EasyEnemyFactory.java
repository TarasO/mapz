package sample;

public class EasyEnemyFactory implements AbstractEnemyFactory {

    @Override
    public Enemy createEnemy() {
        return GreenbatFactory.getInstance().createEnemy();
    }

    @Override
    public EnemyBoss createBoss() {
        return new EnemyBossEasyVioletUfo();
    }
}
