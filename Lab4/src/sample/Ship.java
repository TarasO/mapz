package sample;

import javafx.scene.canvas.GraphicsContext;

import java.util.List;

public abstract class Ship {
    protected Sprite sprite;
    protected int life;

    Ship(){}

    public void update(double time) {
        this.sprite.update(time);
    }

    public void render(GraphicsContext gc) {
        this.sprite.render(gc);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }
}
