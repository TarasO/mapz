package sample;

public abstract class EnemyBoss extends Enemy {

    public void moveToPlayer(PlayerShip player) {
        if((this.sprite.getPositionY()+170) < player.getSprite().getPositionY() && (this.sprite.getPositionX()+90) < player.getSprite().getPositionX()) {
            this.sprite.setVelocity(0, 0);
            this.sprite.addVelocity(65,65);
        } else if((this.sprite.getPositionY()+170) < player.getSprite().getPositionY() && (this.sprite.getPositionX()+90) > player.getSprite().getPositionX()){
            this.sprite.setVelocity(0, 0);
            this.sprite.addVelocity(-65,65);
        } else if((this.sprite.getPositionY()+170) > player.getSprite().getPositionY() && (this.sprite.getPositionX()+90) < player.getSprite().getPositionX()) {
            this.sprite.setVelocity(0, 0);
            this.sprite.addVelocity(65,-65);
        } else {
            this.sprite.setVelocity(0, 0);
            this.sprite.addVelocity(-65,-65);
        }
    }
}
