package sample;

import javafx.scene.image.Image;

import java.util.ArrayList;

public class SingleGun extends Gun {

    SingleGun() {
        this.bullets = new ArrayList<>();
        image = SpriteImageHolder.singleBullet;
        damage = 10;
        cooldown = 0;
    }

    public void shoot(PlayerShip player) {
        Sprite bullet = new Sprite();
        bullet.setImage(image);
        bullet.setPosition(player.sprite.getPositionX()+20, player.sprite.getPositionY()-25);
        bullet.addVelocity(0,-300);
        this.bullets.add(bullet);
    }


}
