package sample;

public abstract class Enemy extends Ship {

    protected String image;
    protected int damage;

    Enemy(){}

    Enemy(Enemy target) {
        this.sprite = target.sprite;
        this.life = target.life;
        this.damage = target.damage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Enemy clone() { return null; }
}
