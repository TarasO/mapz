package sample;

import javafx.scene.image.Image;

import java.util.List;

public abstract class Gun implements Shootable {

    protected Image image;
    protected List<Sprite> bullets;
    protected int damage;
    protected int cooldown;

    public boolean hit(List<Enemy> enemies) {

        for(int i = 0; i < bullets.size(); i++) {
            Sprite bullet = bullets.get(i);

            for(int j = 0; j < enemies.size(); j++) {
                Sprite enemy = enemies.get(j).sprite;
                if(bullet.intersects(enemy)) {
                    bullets.remove(i);
                    i--;
                    enemies.get(j).setLife(enemies.get(j).getLife() - this.damage);
                    if(enemies.get(j).getLife() == 0) {
                        enemies.remove(j);
                        j--;
                    }

                    Main.killedEnemies++;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hit(EnemyBoss enemyBoss) {
        for(int i = 0; i < bullets.size(); i++) {
            Sprite bullet = bullets.get(i);

            if(bullet.intersects(enemyBoss.sprite)) {
                bullets.remove(i);
                enemyBoss.setLife(enemyBoss.getLife() - this.damage);
                return true;
            }
        }
        return false;
    }

    public void shoot(PlayerShip player) { }

    public List<Sprite> getBullets() {
        return bullets;
    }

    public void setBullets(List<Sprite> bullets) {
        this.bullets = bullets;
    }

    public int getDamage() {
        return damage;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }
}
