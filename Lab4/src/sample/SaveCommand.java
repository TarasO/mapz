package sample;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class SaveCommand {

    private String name;
    private String difficulty;
    private int score;

    public SaveCommand(String name, String difficulty, int score) {
        this.name = name;
        this.difficulty = difficulty;
        this.score = score;
    }

    public void execute() {
        HttpClient httpClient = new DefaultHttpClient();

        try {
            HttpPost request = new HttpPost("http://localhost:8080/scores");
            StringEntity params = new StringEntity("{\"name\":\"" + name + "\",\"difficulty\":\"" + difficulty + "\",\"score\":\"" + score +"\"}");
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
        } catch (Exception e) {

        } finally {
            httpClient.getConnectionManager().shutdown();
        }
    }
}
