package sample;

import javafx.scene.image.Image;

public class EnemyRedhawk extends Enemy {

    EnemyRedhawk() {
        this.sprite = new Sprite();
        this.sprite.setImage(SpriteImageHolder.redHawk);
        this.sprite.setPosition(1000*Math.random() + 100,-100*Math.random());
        this.sprite.addVelocity(0, 90);

        this.life = 30;
        this.damage = 30;
    }

    EnemyRedhawk(Enemy target) {
        super(target);
    }

    public Enemy clone() {
        return new EnemyRedhawk(this);
    }
}
