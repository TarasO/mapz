package sample;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.util.List;

public class GetCommand implements Command {

    private String difficulty;
    private List<Score> scores;

    public GetCommand(List<Score> scores, String difficulty) {
        this.scores = scores;
        this.difficulty = difficulty;
    }

    @Override
    public void execute() {
        HttpClient httpClient = new DefaultHttpClient();

        try {
            HttpGet request = new HttpGet("http://localhost:8080/scores/difficulty/" + difficulty.toLowerCase());
            HttpResponse response = httpClient.execute(request);
            InputStream inputStream = response.getEntity().getContent();
            String content = new String(IOUtils.toByteArray(inputStream), "UTF-8");
            String[] contentScores = content.split("},");
            for(String score : contentScores) {
                String[] parts = score.split(",");
                String name = parts[1].split(":")[1].replaceAll("\"","");
                int playerScore = Integer.valueOf(parts[3].split(":")[1].replaceAll("}]",""));
                scores.add(new Score(name, difficulty, playerScore));
            }
        } catch(Exception e) {

        } finally {
            httpClient.getConnectionManager().shutdown();
        }
    }

    public List<Score> getScores() {
        return scores;
    }

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }
}
