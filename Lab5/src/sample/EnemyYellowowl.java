package sample;

import javafx.scene.image.Image;

public class EnemyYellowowl extends Enemy {

    EnemyYellowowl() {
        this.sprite = new Sprite();
        this.sprite.setImage(SpriteImageHolder.yellowOwl);
        this.sprite.setPosition(1000*Math.random() + 100,-100*Math.random());
        this.sprite.addVelocity(0, 75);

        this.life = 20;
        this.damage = 20;
    }
}
