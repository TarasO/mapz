package sample;

import java.util.List;

public interface Shootable {

    public boolean hit(List<Enemy> enemies);
    public boolean hit(EnemyBoss enemyBoss);
    public void shoot(PlayerShip player);

}
