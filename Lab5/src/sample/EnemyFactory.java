package sample;

public interface EnemyFactory {

    public Enemy createEnemy();
}
