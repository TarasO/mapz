package sample;

import javafx.scene.image.Image;

import java.util.List;

public class PlayerShip extends Ship {

    private Armament armament;

    PlayerShip(Armament armament) {
        this.sprite = new Sprite();
        this.sprite.setImage(SpriteImageHolder.player);
        this.sprite.setPosition(630,670);
        this.life = 100;
        this.armament = armament;
    }

    public Armament getArmament() {
        return armament;
    }

    public void setArmament(Armament armament) {
        this.armament = armament;
    }

    public boolean hit(List<Enemy> enemies) {
        return armament.hit(enemies);
    }

    public boolean hitBody(Enemy enemy) {
        return this.sprite.intersects(enemy.sprite);
    }

    public PlayerMemento createMemento() {
        return new PlayerMemento(this);
    }

    public void restore(PlayerMemento memento) {
        this.sprite.setPosition(memento.getPositionX(), memento.getPositionY());
        this.life = memento.getLife();
        this.armament = memento.getArmament();

    }
}
