package sample;

public interface AbstractEnemyFactory {

    Enemy createEnemy();
    EnemyBoss createBoss();
}
