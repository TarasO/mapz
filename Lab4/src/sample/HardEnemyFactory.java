package sample;

public class HardEnemyFactory implements AbstractEnemyFactory {

    @Override
    public Enemy createEnemy() {
        return RedhawkFactory.getInstance().createEnemy();
    }

    @Override
    public EnemyBoss createBoss() {
        return new EnemyBossHardVioletUfo();
    }
}
