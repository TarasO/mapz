package sample;

import javafx.scene.image.Image;

public class SpriteImageHolder {

    public static Image player = new Image(Main.class.getClassLoader().getResource("player.png").toString());

    public static Image greenBat = new Image(Main.class.getClassLoader().getResource("greenbat.png").toString());
    public static Image yellowOwl = new Image(Main.class.getClassLoader().getResource("yellowowl.png").toString());
    public static Image redHawk = new Image(Main.class.getClassLoader().getResource("redhawk.png").toString());

    public static Image violetUfo = new Image(Main.class.getClassLoader().getResource("violetufo.png").toString());

    public static Image clock = new Image(Main.class.getClassLoader().getResource("clock.png").toString());
    public static Image greyClock = new Image(Main.class.getClassLoader().getResource("grey_clock.png").toString());

    public static Image singleBullet = new Image(Main.class.getClassLoader().getResource("bullet.png").toString());
}
