package sample;

public class PlayerMemento {

    private double x;
    private double y;
    private int life;
    private Armament armament;

    public PlayerMemento(PlayerShip player) {
        this.x = player.getSprite().getPositionX();
        this.y = player.getSprite().getPositionY();
        this.life = player.getLife();
        this.armament = player.getArmament();
    }

    public double getPositionX() {
        return x;
    }

    public double getPositionY() {
        return y;
    }

    public int getLife() {
        return life;
    }

    public Armament getArmament() {
        return armament;
    }
}
