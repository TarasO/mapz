package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main extends Application {

    static DifficultyStrategy strategy;
    static String difficulty;
    static int killedEnemies = 0;

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Space Shooter");
        showMainMenu(primaryStage);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public static void startGame(Stage primaryStage) {
        Group root = new Group();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        Canvas canvas = new Canvas( 1280, 720);
        root.getChildren().add(canvas);

        ArrayList<String> input = new ArrayList<String>();

        scene.setOnKeyPressed(
                new EventHandler<KeyEvent>()
                {
                    public void handle(KeyEvent e)
                    {
                        String code = e.getCode().toString();

                        if (!input.contains(code))
                            input.add(code);
                    }
                });

        scene.setOnKeyReleased(
                new EventHandler<KeyEvent>()
                {
                    public void handle(KeyEvent e)
                    {
                        String code = e.getCode().toString();
                        input.remove(code);
                    }
                });

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Font font = Font.font( "Helvetica", FontWeight.BOLD, 24 );
        gc.setFont(font);
        gc.setLineWidth(1);

        Image space = new Image(Main.class.getClassLoader().getResource("space.jpg").toString());

        PlayerShip player = new PlayerShip(new Armament(new SingleGun()));

        MementoWrapper mementoWrapper = new MementoWrapper(player.createMemento());
        IntValue mementoTimer = new IntValue(1000);
        IntValue mementoCooldown = new IntValue(0);

        List<Enemy> enemies = new ArrayList<>();
        BossWrapper bossWrapper = new BossWrapper();

        LongValue lastNanoTime = new LongValue(System.nanoTime());
        IntValue score = new IntValue(0);

//        EncryptedFileWriter encryptedFileWriter = new EncryptedFileWriter("score.txt");
//
//        String prevScore = encryptedFileWriter.readFile();

//        Runtime.getRuntime().addShutdownHook(new Thread() {
//            public void run() {
//                encryptedFileWriter.writeFile(String.valueOf(score.value));
//            }
//        });

        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                gc.setFill(Color.YELLOW);

                double elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
                lastNanoTime.value = currentNanoTime;

                gc.drawImage( space, 0, 0 );

//                if(score.value > 10000) {
//                    enemyFactory = YellowowlFactory.getInstance();
//                }

                if(mementoTimer.value > 0) {
                    mementoTimer.value -= 2;
                } else {
                    mementoWrapper.setMemento(player.createMemento());
                    mementoTimer.value = 1000;
                }

                if(mementoCooldown.value > 0) {
                    mementoCooldown.value -= 2;
                }

                if(killedEnemies < 20) {
                    strategy.execute(enemies);
                }

                if(bossWrapper.getBoss() == null) {
                    bossWrapper.setBoss(strategy.execute(killedEnemies));
                } else {
                    bossWrapper.getBoss().update(elapsedTime);
                    bossWrapper.getBoss().render(gc);
                    bossWrapper.getBoss().moveToPlayer(player);

                    if(player.getArmament().hit(bossWrapper.getBoss())) {
                        score.value += 45;
                    }

                    if(player.hitBody(bossWrapper.getBoss())) {
                        player.setLife(player.getLife() - bossWrapper.getBoss().getDamage());
                    }

                    if (bossWrapper.getBoss().getLife() <= 0) {
                        bossWrapper.setBoss(null);
                        killedEnemies = 0;
                    }
                }

                Iterator<Enemy> enemiesIter = enemies.iterator();
                while(enemiesIter.hasNext()){
                    Enemy enemy = enemiesIter.next();
                    enemy.update(elapsedTime);
                    enemy.render(gc);
                    if(enemy.getSprite().getPositionY() > 750) {
                        enemiesIter.remove();
                    }

                    if(player.hitBody(enemy)){
                        enemiesIter.remove();
                        player.setLife(player.getLife() - enemy.getDamage());
                    }
                }

                if(player.getLife() <= 0) {
                    killedEnemies = 0;
                    score.value = strategy.difficultyMultiplier(score.value);
                    this.stop();
                    primaryStage.close();
                    showGameOver(primaryStage, score.value);
                }

                player.getSprite().setVelocity(0,0);
                if (input.contains("LEFT") || input.contains("A"))
                    player.getSprite().addVelocity(-250,0);
                if (input.contains("RIGHT") || input.contains("D"))
                    player.getSprite().addVelocity(250,0);
                if (input.contains("UP") || input.contains("W"))
                    player.getSprite().addVelocity(0,-250);
                if (input.contains("DOWN") || input.contains("S"))
                    player.getSprite().addVelocity(0,250);
                if (input.contains("SPACE")){
                    player.getArmament().shoot(player);
                }
                if (input.contains("DIGIT1") && mementoCooldown.value == 0) {
                    player.restore(mementoWrapper.getMemento());
                    mementoCooldown.value = 1000;
                }
                if (input.contains("ESCAPE")) {
                    player.setLife(0);
                }

                for(int i = 0; i < player.getArmament().getBullets().size(); i++) {
                    Sprite bullet = player.getArmament().getBullets().get(i);
                    bullet.update(elapsedTime);
                    bullet.render(gc);

                    if(bullet.getPositionY() < 0) {
                        player.getArmament().getBullets().remove(i);
                    }
                }

                if(player.getArmament().hit(enemies)) {
                    score.value += 20;
                }

                player.update(elapsedTime);
                player.render(gc);

                player.getArmament().reduceCooldown(5);

                String lifeText = "Life:" + player.getLife();
                gc.fillText(lifeText, 100,30);
                String pointsText = "Score:" + score.value;
                gc.fillText(pointsText, 1100, 30);
                gc.fillText(String.valueOf(mementoWrapper.getMemento().getLife()), 1130, 620);
                if(mementoCooldown.value == 0) {
                    gc.drawImage(SpriteImageHolder.clock, 1150, 640);
                } else {
                    gc.drawImage(SpriteImageHolder.greyClock, 1150, 640);
                }

//                if(prevScore != null) {
//                    gc.fillText("Prev. Score:" + prevScore, 1070, 60);
//                }
            }
        }.start();
        primaryStage.show();
    }

    public static void showMainMenu(Stage primaryStage) {
        Pane mainRoot = new Pane();
        mainRoot.setPrefSize(1280, 720);

        Image bg = new Image(Main.class.getClassLoader().getResource("space.jpg").toString());
        ImageView imageView = new ImageView(bg);
        imageView.setFitWidth(1280);
        imageView.setFitHeight(720);

        Button startButton = new Button("Start game");
        startButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        startButton.setPrefWidth(500);
        startButton.setFont(Font.font(50));
        startButton.setOnAction(event -> showDifficultySelect(primaryStage));

        Button leaderboardsButton = new Button("Leaderboards");
        leaderboardsButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        leaderboardsButton.setPrefWidth(500);
        leaderboardsButton.setFont(Font.font(50));
        leaderboardsButton.setOnAction(event -> showLeaderboards(primaryStage));

        Button exitButton = new Button("Exit game");
        exitButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        exitButton.setPrefWidth(500);
        exitButton.setFont(Font.font(50));
        exitButton.setOnAction(event -> System.exit(0));

        VBox vBox = new VBox(50, startButton, leaderboardsButton, exitButton);
        vBox.setTranslateX(400);
        vBox.setTranslateY(150);

        mainRoot.getChildren().addAll(imageView, vBox);
        Scene mainScene = new Scene(mainRoot);
        primaryStage.setScene(mainScene);
    }

    public static void showDifficultySelect(Stage primaryStage) {
        Pane difficultyRoot = new Pane();
        difficultyRoot.setPrefSize(1280, 720);

        Image bg = new Image(Main.class.getClassLoader().getResource("space.jpg").toString());
        ImageView imageView = new ImageView(bg);
        imageView.setFitWidth(1280);
        imageView.setFitHeight(720);

        Button backButton = new Button("Back");
        backButton.setLayoutX(25);
        backButton.setLayoutY(25);
        backButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        backButton.setFont(Font.font(30));
        backButton.setOnAction(event -> showMainMenu(primaryStage));

        Button easyButton = new Button("Easy");
        easyButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        easyButton.setPrefWidth(500);
        easyButton.setFont(Font.font(50));
        easyButton.setOnAction(event -> {
            strategy = new EasyDifficultyStrategy();
            difficulty = "Easy";
            primaryStage.close();
            startGame(primaryStage);
        });

        Button mediumButton = new Button("Medium");
        mediumButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        mediumButton.setPrefWidth(500);
        mediumButton.setFont(Font.font(50));
        mediumButton.setOnAction(event -> {
            strategy = new MediumDifficultyStrategy();
            difficulty = "Medium";
            primaryStage.close();
            startGame(primaryStage);
        });

        Button hardButton = new Button("Hard");
        hardButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        hardButton.setPrefWidth(500);
        hardButton.setFont(Font.font(50));
        hardButton.setOnAction(event -> {
            strategy = new HardDifficultyStrategy();
            difficulty = "Hard";
            primaryStage.close();
            startGame(primaryStage);
        });

        VBox vBox = new VBox(50, easyButton, mediumButton, hardButton);
        vBox.setTranslateX(400);
        vBox.setTranslateY(150);

        difficultyRoot.getChildren().addAll(imageView, backButton, vBox);
        Scene difficultyScene = new Scene(difficultyRoot);
        primaryStage.setScene(difficultyScene);
    }

    public static void showLeaderboards(Stage primaryStage) {
        Pane leaderboardsRoot = new Pane();
        leaderboardsRoot.setPrefSize(1280, 720);

        Image bg = new Image(Main.class.getClassLoader().getResource("space.jpg").toString());
        ImageView imageView = new ImageView(bg);
        imageView.setFitWidth(1280);
        imageView.setFitHeight(720);

        Button backButton = new Button("Back");
        backButton.setLayoutX(25);
        backButton.setLayoutY(25);
        backButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        backButton.setFont(Font.font(30));
        backButton.setOnAction(event -> showMainMenu(primaryStage));

        TabPane tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        TableColumn<Score, String> easyNameColumn = new TableColumn<>("Name");
        easyNameColumn.setCellValueFactory(new PropertyValueFactory<Score, String>("name"));
        TableColumn<Score, Integer> easyScoreColumn = new TableColumn<>("Score");
        easyScoreColumn.setCellValueFactory(new PropertyValueFactory<Score, Integer>("score"));
        List<Score> easyScores = new ArrayList<>();
        new GetCommand(easyScores, "Easy").execute();
        ObservableList<Score> observableEasyScores = FXCollections.observableArrayList(easyScores);
        TableView<Score> easyTable = new TableView<Score>(observableEasyScores);
        easyTable.getColumns().addAll(easyNameColumn, easyScoreColumn);
        Tab easyTab = new Tab("Easy", easyTable);

        TableColumn<Score, String> mediumNameColumn = new TableColumn<>("Name");
        mediumNameColumn.setCellValueFactory(new PropertyValueFactory<Score, String>("name"));
        TableColumn<Score, Integer> mediumScoreColumn = new TableColumn<>("Score");
        mediumScoreColumn.setCellValueFactory(new PropertyValueFactory<Score, Integer>("score"));
        List<Score> mediumScores = new ArrayList<>();
        new GetCommand(mediumScores, "Medium").execute();
        ObservableList<Score> observableMediumScores = FXCollections.observableArrayList(mediumScores);
        TableView<Score> mediumTable = new TableView<Score>(observableMediumScores);
        mediumTable.getColumns().addAll(mediumNameColumn, mediumScoreColumn);
        Tab mediumTab = new Tab("Medium", mediumTable);

        TableColumn<Score, String> hardNameColumn = new TableColumn<>("Name");
        hardNameColumn.setCellValueFactory(new PropertyValueFactory<Score, String>("name"));
        TableColumn<Score, Integer> hardScoreColumn = new TableColumn<>("Score");
        hardScoreColumn.setCellValueFactory(new PropertyValueFactory<Score, Integer>("score"));
        List<Score> hardScores = new ArrayList<>();
        new GetCommand(hardScores, "Hard").execute();
        ObservableList<Score> observableHardScores = FXCollections.observableArrayList(hardScores);
        TableView<Score> hardTable = new TableView<Score>(observableHardScores);
        hardTable.getColumns().addAll(hardNameColumn, hardScoreColumn);
        Tab hardTab = new Tab("Hard", hardTable);

        tabPane.getTabs().addAll(easyTab, mediumTab, hardTab);

        VBox vBox = new VBox(tabPane);
        vBox.setTranslateX(500);
        vBox.setTranslateY(150);

        leaderboardsRoot.getChildren().addAll(imageView, backButton, vBox);
        Scene leaderboardsScene = new Scene(leaderboardsRoot);
        primaryStage.setScene(leaderboardsScene);
    }

    public static void showGameOver(Stage primaryStage, int score) {
        Pane gameOverRoot = new Pane();
        gameOverRoot.setPrefSize(1280, 720);

        Image bg = new Image(Main.class.getClassLoader().getResource("space.jpg").toString());
        ImageView imageView = new ImageView(bg);
        imageView.setFitWidth(1280);
        imageView.setFitHeight(720);

        Button backButton = new Button("Main menu");
        backButton.setLayoutX(25);
        backButton.setLayoutY(25);
        backButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        backButton.setFont(Font.font(30));
        backButton.setOnAction(event -> showMainMenu(primaryStage));

        Label gameOver = new Label("Game Over");
        gameOver.setFont(Font.font("Helvetica", 80));
        gameOver.setTextFill(Color.YELLOW);

        Label scoreLabel = new Label("Score: " + score);
        scoreLabel.setFont(Font.font("Helvetica", 40));
        scoreLabel.setTextFill(Color.YELLOW);
        scoreLabel.setPadding(new Insets(0, 0, 0, 100));

        TextField textField = new TextField();

        Button sendButton = new Button("Send");
        sendButton.setStyle("-fx-base: yellow; -fx-background-radius: 50px;");
        sendButton.setPrefWidth(500);
        sendButton.setFont(Font.font(30));
        sendButton.setOnAction(event -> new SaveCommand(textField.getText(), difficulty, score).execute());

        VBox vBox = new VBox(30, gameOver, scoreLabel, textField, sendButton);
        vBox.setTranslateX(450);
        vBox.setTranslateY(150);

        gameOverRoot.getChildren().addAll(imageView, backButton, vBox);
        Scene gameOverScene = new Scene(gameOverRoot);
        primaryStage.setScene(gameOverScene);
        primaryStage.show();
    }
}
