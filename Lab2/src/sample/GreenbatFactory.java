package sample;

public class GreenbatFactory implements EnemyFactory {

    private static GreenbatFactory instance;

    @Override
    public Enemy createEnemy() {
        return new EnemyGreenbat();
    }

    public static GreenbatFactory getInstance() {
        if(instance == null) {
            instance = new GreenbatFactory();
        }
        return instance;
    }
}
