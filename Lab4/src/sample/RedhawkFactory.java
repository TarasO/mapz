package sample;

public class RedhawkFactory implements EnemyFactory {

    private static RedhawkFactory instance;

    @Override
    public Enemy createEnemy() {
        return new EnemyRedhawk();
    }

    public static RedhawkFactory getInstance() {
        if(instance == null) {
            instance = new RedhawkFactory();
        }
        return instance;
    }
}
