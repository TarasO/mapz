package sample;

public class EnemyBossEasyVioletUfo extends EnemyBoss {

    public EnemyBossEasyVioletUfo() {
        this.sprite = new Sprite();
        this.sprite.setImage(SpriteImageHolder.violetUfo);
        this.sprite.setPosition(640, 0);
        this.sprite.addVelocity(0, 0);

        this.life = 300;
        this.damage = 40;
    }
}
