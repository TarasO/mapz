package sample;

public class EnemyBossHardVioletUfo extends EnemyBoss {

    public EnemyBossHardVioletUfo() {
        this.sprite = new Sprite();
        this.sprite.setImage(SpriteImageHolder.violetUfo);
        this.sprite.setPosition(640, -200);
        this.sprite.addVelocity(0, 0);

        this.life = 800;
        this.damage = 100;
    }
}
