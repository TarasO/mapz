package sample;

import java.util.List;

public class Armament implements Shootable {

    private Gun gun;

    public Armament(Gun gun) {
        this.gun = gun;
    }

    @Override
    public boolean hit(List<Enemy> enemies) {
        return gun.hit(enemies);
    }

    @Override
    public void shoot(PlayerShip player) {
        if(gun.getCooldown() == 0) {
            gun.shoot(player);
            gun.setCooldown(100);
        }
    }

    public List<Sprite> getBullets() {
        return gun.getBullets();
    }

    public void reduceCooldown(int num) {
        if(gun.getCooldown() > 0) {
            gun.setCooldown(gun.getCooldown() - num);
        }
    }
}
