package sample;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Base64;
import java.util.Scanner;

public class EncryptedFileWriter {

    String filename;

    EncryptedFileWriter(String filename) {
        this.filename = filename;
    }

    public void writeFile(String str) {
        try {
            SecretKeySpec aesKey = new SecretKeySpec("lolkekcheburekkk".getBytes(), "AES");
            Cipher aesCipher = Cipher.getInstance("AES");

            byte[] text = str.getBytes("UTF8");

            aesCipher.init(Cipher.ENCRYPT_MODE, aesKey);

            byte[] textEncrypted = aesCipher.doFinal(text);

            FileWriter fileWriter = new FileWriter(filename);
            fileWriter.write(Base64.getEncoder().encodeToString(textEncrypted));
            fileWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readFile() {
        try {
            SecretKeySpec aesKey = new SecretKeySpec("lolkekcheburekkk".getBytes(), "AES");
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.DECRYPT_MODE, aesKey);

            FileReader fileReader = new FileReader(filename);
            Scanner scanner = new Scanner(fileReader);

            byte[] textEncrypted = scanner.nextLine().getBytes();
            scanner.close();
            fileReader.close();

            byte[] textDecrypted = aesCipher.doFinal(Base64.getDecoder().decode(textEncrypted));
            return new String(textDecrypted);
        } catch (FileNotFoundException e){
            return null;
        }catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
