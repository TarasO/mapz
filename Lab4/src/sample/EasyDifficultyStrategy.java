package sample;

import java.util.List;

public class EasyDifficultyStrategy implements DifficultyStrategy {

    private AbstractEnemyFactory enemyFactory;

    EasyDifficultyStrategy() {
        enemyFactory = new EasyEnemyFactory();
    }

    @Override
    public void execute(List<Enemy> enemies) {
        if (enemies.size() < 5) {
            enemies.add(enemyFactory.createEnemy());
        }
    }

    @Override
    public EnemyBoss execute(int killedEnemies) {
        if(killedEnemies >= 20) {
            return enemyFactory.createBoss();
        }
        return null;
    }

    @Override
    public int difficultyMultiplier(int score) {
        return score * 1;
    }
}
