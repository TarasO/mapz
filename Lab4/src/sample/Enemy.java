package sample;

public abstract class Enemy extends Ship {

    protected String image;
    protected int damage;

    Enemy(){}

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
