package sample;

import javafx.scene.image.Image;

public class EnemyGreenbat extends Enemy{

    EnemyGreenbat() {
        this.sprite = new Sprite();
        this.sprite.setImage(SpriteImageHolder.greenBat);
        this.sprite.setPosition(1000*Math.random() + 100,-100*Math.random());
        this.sprite.addVelocity(0, 100);

        this.life = 10;
        this.damage = 10;
    }
}
