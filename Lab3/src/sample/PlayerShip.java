package sample;

import javafx.scene.image.Image;

import java.util.List;

public class PlayerShip extends Ship {

    private Armament armament;

    PlayerShip(Armament armament) {
        this.sprite = new Sprite();
        this.sprite.setImage(SpriteImageHolder.player);
        this.sprite.setPosition(630,720);
        this.life = 100;
        this.armament = armament;
    }

    public Armament getArmament() {
        return armament;
    }

    public void setArmament(Armament armament) {
        this.armament = armament;
    }

    public boolean hit(List<Enemy> enemies) {
        return armament.hit(enemies);
    }

    public boolean hitBody(Enemy enemy) {
        return this.sprite.intersects(enemy.sprite);
    }
}
