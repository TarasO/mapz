package sample;

public class RedhawkFactory implements EnemyFactory {

    private static Enemy redHawk = new EnemyRedhawk();
    private static RedhawkFactory instance;

    @Override
    public Enemy createEnemy() {
        return redHawk.clone();
    }

    public static RedhawkFactory getInstance() {
        if(instance == null) {
            instance = new RedhawkFactory();
        }
        return instance;
    }
}
