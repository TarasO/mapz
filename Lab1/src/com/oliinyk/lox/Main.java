package com.oliinyk.lox;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    private static final Interpreter interpreter = new Interpreter();

    static boolean hadError = false;
    static boolean hadRuntimeError = false;

    public static JTextArea treeArea;
    public static JTextArea resultArea;

    public static void main(String[] args) {

        JFrame frame = new JFrame("Interpreter");
        frame.setSize(1024,768);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        JPanel treePanel = new JPanel();
        treePanel.setLayout(new BoxLayout(treePanel, BoxLayout.PAGE_AXIS));
        treePanel.add(new JLabel("Tree:"));
        treePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        treeArea = new JTextArea(35,25);
        JScrollPane treePane = new JScrollPane(treeArea);
        treePanel.add(treePane);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.PAGE_AXIS));
        inputPanel.add(Box.createRigidArea(new Dimension(0, 35)));
        inputPanel.add(new JLabel("Input:"));
        inputPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        JTextArea inputArea = new JTextArea(35, 25);
        JScrollPane inputPane = new JScrollPane(inputArea);
        inputPanel.add(inputPane);
        inputPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        JButton runButton = new JButton("Run");
        runButton.addActionListener(e -> {
            run(inputArea.getText());
        });
        JButton readButton = new JButton("Read");
        readButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            int returnVal = fileChooser.showOpenDialog(inputPanel);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();

                String data = "";
                try {
                    data = new String(Files.readAllBytes(Paths.get(file.getPath())));
                    inputArea.setText(data);
                } catch (IOException exception) {
                    resultArea.append("Error opening file\n");
                }
            } else {
                resultArea.append("Open command cancelled by user\n");
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(runButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(50, 0)));
        buttonPanel.add(readButton);

        inputPanel.add(buttonPanel);

        JPanel resultPanel = new JPanel();
        resultPanel.setLayout(new BoxLayout(resultPanel, BoxLayout.PAGE_AXIS));
        resultPanel.add(Box.createRigidArea(new Dimension(0, 35)));
        resultPanel.add(new JLabel("Result:"));
        resultPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        resultArea = new JTextArea(35, 25);
        JScrollPane resultPane = new JScrollPane(resultArea);
        resultPanel.add(resultPane);
        resultPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        JButton clearButton = new JButton("Clear all");
        clearButton.addActionListener(e -> {
            treeArea.setText("");
//            inputArea.setText("");
            resultArea.setText("");
        });
        resultPanel.add(clearButton);

        panel.add(treePanel);
        panel.add(inputPanel);
        panel.add(resultPanel);

        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);

    }

    private static void runFile(String path) throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(path));
        run(new String(bytes, Charset.defaultCharset()));

        if (hadError) System.exit(65);
        if (hadRuntimeError) System.exit(70);
    }

    private static void runPrompt() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);

        for (;;) {
            System.out.print("> ");
            run(reader.readLine());
            hadError = false;
        }
    }

    private static void run(String source) {
        Scanner scanner = new Scanner(source);
        List<Token> tokens = scanner.scanTokens();

        Parser parser = new Parser(tokens);
        List<Stmt> statements = parser.parse();

        if(hadError) return;

        AstPrinter printer = new AstPrinter();
        statements.forEach(stmt -> {
            treeArea.append(printer.print(stmt));
        });

        Resolver resolver = new Resolver(interpreter);
        resolver.resolve(statements);

        if (hadError) return;

        interpreter.interpret(statements);
    }

    static void error(int line, String message) {
        report(line, "", message);
    }

    static void error(Token token, String message) {
        if(token.type == TokenType.EOF) {
            report(token.line, " at end", message);
        } else {
            report(token.line, " at '" + token.lexeme + "'", message);
        }
    }

    private static void report(int line, String where, String message) {
        resultArea.append("[line " + line + "] Error" + where + ": " + message + "\n");
        hadError = true;
    }

    static void runtimeError(RuntimeError error) {
        resultArea.append(error.getMessage() + "\n[line " + error.token.line + "]\n");
        hadRuntimeError = true;
    }
}
