package sample;

public class BossWrapper {

    private EnemyBoss boss;

    public BossWrapper() {
    }

    public BossWrapper(EnemyBoss boss) {
        this.boss = boss;
    }

    public EnemyBoss getBoss() {
        return boss;
    }

    public void setBoss(EnemyBoss boss) {
        this.boss = boss;
    }
}
