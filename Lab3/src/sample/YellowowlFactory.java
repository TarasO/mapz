package sample;

public class YellowowlFactory implements EnemyFactory {

    @Override
    public Enemy createEnemy() {
        return new EnemyYellowowl();
    }

    private static class SingletonHolder {
        private static YellowowlFactory instance = new YellowowlFactory();
    }

    public static YellowowlFactory getInstance() {
        return SingletonHolder.instance;
    }

}
