package com.oliinyk.lox;

import java.util.Collection;
import java.util.Iterator;

class AstPrinter implements Expr.Visitor<String>, Stmt.Visitor<String> {

    private static int indent = 0;

    String print(Expr expr) {
        return expr.accept(this);
    }

    String print(Stmt stmt) {
        return stmt.accept(this);
    }

    @Override
    public String visitBlockStmt(Stmt.Block stmt) {
        StringBuilder builder = new StringBuilder();
        builder.append("block");

        indent++;
        for (Stmt statement : stmt.statements) {
            builder.append("\n");
            for(int i = 0; i < indent; i++) { builder.append("          "); }
            builder.append(statement.accept(this));
        }

        indent--;
        return builder.toString();
    }

    @Override
    public String visitClassStmt(Stmt.Class stmt) {
        StringBuilder builder = new StringBuilder();
        builder.append("(class " + stmt.name.lexeme);

        if (stmt.superclass != null) {
            builder.append(" < " + print(stmt.superclass));
        }

        for (Stmt.Function method : stmt.methods) {
            builder.append(" " + print(method));
        }

        builder.append(")");
        return builder.toString();
    }

    @Override
    public String visitExpressionStmt(Stmt.Expression stmt) {
        return parenthesize("", stmt.expression);
    }

    @Override
    public String visitFunctionStmt(Stmt.Function stmt) {
        StringBuilder builder = new StringBuilder();
        builder.append("fun " + stmt.name.lexeme + "\n");

        indent++;
        for (Token param : stmt.params) {
            for(int i = 0; i < indent; i++) { builder.append("          "); }
            builder.append(param.lexeme);
            builder.append("\n");
        }

        builder.append("\n");

        for (Stmt body : stmt.body) {
            for(int i = 0; i < indent; i++) { builder.append("          "); }
            builder.append(body.accept(this));
        }

        indent--;
        return builder.toString();
    }

    @Override
    public String visitIfStmt(Stmt.If stmt) {
        if (stmt.elseBranch == null) {
            return parenthesize2("if", stmt.condition, stmt.thenBranch);
        }

        return parenthesize2("if-else", stmt.condition, stmt.thenBranch,
                stmt.elseBranch);
    }

    @Override
    public String visitPrintStmt(Stmt.Print stmt) {
        return parenthesize("print", stmt.expression);
    }

    @Override
    public String visitImportStmt(Stmt.Import stmt) {
        return parenthesize("import", stmt.expression);
    }

    @Override
    public String visitReturnStmt(Stmt.Return stmt) {
        if (stmt.value == null) return "(return)";
        return parenthesize("return", stmt.value);
    }

    @Override
    public String visitVarStmt(Stmt.Var stmt) {
        if (stmt.initializer == null) {
            return parenthesize2("var", stmt.name);
        }

        return parenthesize2("var " + stmt.name.lexeme + " =", stmt.initializer);
    }

    @Override
    public String visitWhileStmt(Stmt.While stmt) {
        return parenthesize2("while", stmt.condition, stmt.body);
    }

    @Override
    public String visitAssignExpr(Expr.Assign expr) {
        return parenthesize2("=", expr.name.lexeme, expr.value);
    }

    @Override
    public String visitBinaryExpr(Expr.Binary expr) {
        return parenthesize(expr.operator.lexeme, expr.left, expr.right);
    }

    @Override
    public String visitCallExpr(Expr.Call expr) {
        return parenthesize2("call", expr.callee, expr.arguments);
    }


    @Override
    public String visitGetExpr(Expr.Get expr) {
        return parenthesize2(".", expr.object, expr.name.lexeme);
    }

    @Override
    public String visitGroupingExpr(Expr.Grouping expr) {
        return parenthesize("group", expr.expression);
    }

    @Override
    public String visitLiteralExpr(Expr.Literal expr) {
        if (expr.value == null) return "nil";
        if (expr.value.toString().endsWith(".0")) {
            return expr.value.toString().substring(0, expr.value.toString().length()-2);
        }
        if(expr.value instanceof String) {
            return "\"" + expr.value + "\"";
        }
        return expr.value.toString();
    }

    @Override
    public String visitLogicalExpr(Expr.Logical expr) {
        return parenthesize(expr.operator.lexeme, expr.left, expr.right);
    }

    @Override
    public String visitSetExpr(Expr.Set expr) {
        return parenthesize2("=",
                expr.object, expr.name.lexeme, expr.value);
    }

    @Override
    public String visitSuperExpr(Expr.Super expr) {
        return parenthesize2("super", expr.method);
    }

    @Override
    public String visitThisExpr(Expr.This expr) {
        return "this";
    }

    @Override
    public String visitUnaryExpr(Expr.Unary expr) {
        return parenthesize(expr.operator.lexeme, expr.right);
    }

    @Override
    public String visitVariableExpr(Expr.Variable expr) {
        return expr.name.lexeme;
    }

    private String parenthesize(String name, Expr... exprs) {
        StringBuilder builder = new StringBuilder();

        if(name != "") {
            builder.append(name);
            builder.append("\n");
            indent++;
        }

        for (Expr expr : exprs) {
            for(int i = 0; i < indent; i++) { builder.append("          "); }
            builder.append(expr.accept(this));
            builder.append("\n");
        }

        if(name != "") {
            indent--;
        }

        return builder.toString();
    }

    private String parenthesize2(String name, Object... parts) {
        StringBuilder builder = new StringBuilder();

        builder.append(name);

        indent++;
        for (Object part : parts) {

            //FIXME if breaks
            if(part != null) {
                builder.append("\n");
            }
            for(int i = 0; i < indent; i++) { builder.append("             "); }

            //FIXME if breaks
            if(part == null) {
            } else if (part instanceof Expr) {
                builder.append(((Expr) part).accept(this));
            } else if (part instanceof Stmt) {
                builder.append(((Stmt) part).accept(this));
            } else if (part instanceof Token) {
                if(((Token) part).lexeme != null) {
                    builder.append(((Token) part).lexeme);
                } else {
                    builder.append(((Token) part).literal);
                }
            } else if(part instanceof Collection) {
                Iterator iterator = ((Collection) part).iterator();
                indent++;
                while(iterator.hasNext()){
                    for(int i = 0; i < indent; i++) { builder.append("          "); }
                    builder.append(((Expr)iterator.next()).accept(this) + "\n");
                }
                indent--;
            } else {
                builder.append(part);
            }
        }
        indent--;

        return builder.toString();
    }

}