package sample;

public class MementoWrapper {

    private PlayerMemento memento;

    public MementoWrapper() {
    }

    public MementoWrapper(PlayerMemento memento) {
        this.memento = memento;
    }

    public PlayerMemento getMemento() {
        return memento;
    }

    public void setMemento(PlayerMemento memento) {
        this.memento = memento;
    }
}
