package sample;

public class MediumEnemyFactory implements AbstractEnemyFactory {

    @Override
    public Enemy createEnemy() {
        return YellowowlFactory.getInstance().createEnemy();
    }

    @Override
    public EnemyBoss createBoss() {
        return new EnemyBossMediumVioletUfo();
    }
}
